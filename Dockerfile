#
# Yeoman generator for WordPress projects by Wysiwyg Oy
#
# Based on https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
#

FROM node:7-alpine
MAINTAINER Jarno Antikainen <jarno.antikainen@wysiwyg.fi>

LABEL Description="This image runs 'yo generate wysiwygoy-wordpress." Vendor="Wysiwyg Oy"

# Bundle app source
COPY generators /home/node/app/generators
COPY test /home/node/app/test
COPY gulpfile.js /home/node/app/
COPY package.json /home/node/app/
COPY .gitignore /home/node/app/

# Install app dependencies
WORKDIR /home/node/app
RUN apk add --no-cache git && \
    npm install --global yo && \
    npm install && \
    npm link && \
    chown node:node /home/node/app

VOLUME /project
WORKDIR /project
USER node

CMD ["yo", "wysiwygoy-wordpress"]
