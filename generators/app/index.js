'use strict'

// Node dependencies
const Generator = require('yeoman-generator')
const chalk = require('chalk')
const yosay = require('yosay')
const passwordGenerator = require('generate-password')
const dotenv = require('dotenv')

// Configurable file locations

/** @type {String} Site directory (within project root directory). */
const siteDir = 'site'
/** @type {String} Public WWW directory (within site directory). */
const publicDir = 'public'
/** @type {String} WordPress core directory (within public WWW directory). */
const wpCoreDir = 'wordpress'
/** @type {String} WordPress wp-content directory (within public WWW directory). */
const wpContentDir = 'wp-content'
/** @type {String} Absolute path to where site is mapped on Docker containers. */
const containerSitePath = '/site'

// Other configurable parameters
const passwordConfiguration = {
  length: 20,
  number: true,
  symbols: false,
  uppercase: true
}

module.exports = class extends Generator {
  constructor (args, opts) {
    super(args, opts)

    this.argument('projectModel', { type: String, required: true, default: 'wysiwyg' })
  }

  // noinspection JSUnusedGlobalSymbols
  initializing () {
    dotenv.config({
      silent: true
    })

    const wwwDir = siteDir + '/' + publicDir

    this.props = {
      slug: this.appname,
      containerSitePath: containerSitePath,
      hostName: this.appname + '.local.wysiwyg.fi',
      projectDir: process.env.PROJECT_DIR || '.',
      siteDir: siteDir,
      wwwDir: wwwDir,
      containerPublicPath: containerSitePath + '/' + publicDir,
      wpCoreDir: wpCoreDir,
      wpCorePath: wwwDir + '/' + wpCoreDir,
      wpContentDir: wpContentDir,
      wpContentPath: wwwDir + '/' + wpContentDir,
      databaseRootPassword: process.env.DB_ROOT_PASSWORD || passwordGenerator.generate(passwordConfiguration),
      databaseName: process.env.DB_NAME || this.appname + '_wp_dev',
      databaseUser: process.env.DB_USER || this.appname,
      databasePassword: process.env.DB_PASSWORD || passwordGenerator.generate(passwordConfiguration),
      databaseHost: process.env.DB_HOST || this.appname + '-db',
      databasePrefix: process.env.DB_PREFIX || this.options.databasePrefix || 'wp_',
      authKey: process.env.AUTH_KEY || passwordGenerator.generate(passwordConfiguration),
      secureAuthKey: process.env.SECURE_AUTH_KEY || passwordGenerator.generate(passwordConfiguration),
      loggedInKey: process.env.LOGGED_IN_KEY || passwordGenerator.generate(passwordConfiguration),
      nonceKey: process.env.NONCE_KEY || passwordGenerator.generate(passwordConfiguration),
      authSalt: process.env.AUTH_SALT || passwordGenerator.generate(passwordConfiguration),
      secureAuthSalt: process.env.SECURE_AUTH_SALT || passwordGenerator.generate(passwordConfiguration),
      loggedInSalt: process.env.LOGGED_IN_SALT || passwordGenerator.generate(passwordConfiguration),
      nonceSalt: process.env.NONCE_SALT || passwordGenerator.generate(passwordConfiguration),
      deployHost: this.options.deployHost,
      deployUser: this.options.deployUser
    }
  }

  default () {
    switch (this.options.projectModel) {
      case 'wysiwyg':
      case 'traditional':
      case 'bedrock':
        this.composeWith(require.resolve('../' + this.options.projectModel), this.props)
        break
      default:
        // No composing
    }
  }

  // noinspection JSUnusedGlobalSymbols
  prompting () {
    // Have Yeoman greet the user.
    this.log(yosay(
      'Creating a new ' + chalk.red('WordPress project') + ' "' + chalk.yellow(this.appname) + '"'
    ))
  }

  // noinspection JSUnusedGlobalSymbols
  writing () {
    this.fs.copyTpl(this.templatePath('docker-compose.yml'), this.destinationPath('docker-compose.yml'), this.props)
    this.fs.copyTpl(this.templatePath('docker-compose-run.yml'), this.destinationPath('docker-compose-run.yml'), this.props)
    this.fs.copyTpl(this.templatePath('config/nginx/site.conf'), this.destinationPath('config/nginx/site.conf'), this.props)
  }
}
