'use strict'

const Generator = require('yeoman-generator')

module.exports = class extends Generator {
  // noinspection JSUnusedGlobalSymbols
  writing () {
    this.fs.copyTpl(this.templatePath('dotenv'), this.destinationPath('.env'), this.options)
    this.fs.copyTpl(this.templatePath('dotgitignore'), this.destinationPath('.gitignore'), this.options)
    this.fs.copyTpl(this.templatePath('dotgitlab-ci.yml'), this.destinationPath('.gitlab-ci.yml'), this.options)
    this.fs.copyTpl(this.templatePath('composer.json'), this.destinationPath('composer.json'), this.options)
    this.fs.copyTpl(this.templatePath('config/deploy.yml'), this.destinationPath('config/deploy.yml'), this.options)
    this.fs.copyTpl(this.templatePath('config/deploy-files'), this.destinationPath('config/deploy-files'), this.options)
    this.fs.copyTpl(this.templatePath('config/deploy-hosts'), this.destinationPath('config/deploy-hosts'), this.options)
    this.fs.copyTpl(this.templatePath('config/import-files'), this.destinationPath('config/import-files'), this.options)
    this.fs.copyTpl(this.templatePath('site/public/wp-config.php'), this.destinationPath('site/public/wp-config.php'), this.options)
    this.fs.copyTpl(this.templatePath('wp-cli.yml'), this.destinationPath('wp-cli.yml'), this.options)
  }
}
