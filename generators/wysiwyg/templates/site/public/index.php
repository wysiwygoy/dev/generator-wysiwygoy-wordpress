<?php
/**
 * WordPress view bootstrapper.
 *
 * @package <%= slug %>
 */

define( 'WP_USE_THEMES', true );
require( dirname( __FILE__ ) . '/wordpress/wp-blog-header.php' );
