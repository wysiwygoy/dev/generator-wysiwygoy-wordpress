/* eslint-env mocha */

'use strict'
const path = require('path')
const assert = require('yeoman-assert')
const helpers = require('yeoman-test')
const Promise = require('bluebird')
const yaml = Promise.promisifyAll(require('node-yaml'))
const chai = require('chai')
const expect = chai.expect

describe('generator-wysiwygoy-wordpress:wysiwyg', function () {
  before(function () {
    return helpers
      .run(path.join(__dirname, '../../generators/app'))
      .withOptions({
        databasePrefix: 'myprefix',
        deployHost: 'mydeployhost',
        deployUser: 'mydeployuser'
      })
      .withArguments('wysiwyg')
      .toPromise()
  })

  it('creates files', function () {
    assert.file([
      '.env',
      '.gitignore',
      '.gitlab-ci.yml',
      'composer.json',
      'config/deploy.yml',
      'config/deploy-files',
      'config/deploy-hosts',
      'config/nginx/site.conf',
      'docker-compose.yml',
      'docker-compose-run.yml',
      'site/public/wp-config.php',
      'site/public/index.php',
      'wp-cli.yml'
    ])
  })

  it('fills docker-compose.yml with correct content', function () {
    const slug = path.basename(process.cwd())

    return yaml.read(process.cwd() + '/docker-compose.yml').then((dockerComposeYml) => {
      expect(dockerComposeYml.services['nginx'].container_name).to.equal(slug + '-nginx')
      expect(dockerComposeYml.services['nginx'].volumes).to.include('${PROJECT_DIR}/site:/site') // eslint-disable-line no-template-curly-in-string
      expect(dockerComposeYml.services['nginx'].volumes).to.include('${PROJECT_DIR}/config/nginx:/etc/nginx/conf.d') // eslint-disable-line no-template-curly-in-string

      expect(dockerComposeYml.services['php-fpm'].container_name).to.equal(slug + '-php-fpm')
      expect(dockerComposeYml.services['php-fpm'].volumes).to.include('${PROJECT_DIR}/site:/site') // eslint-disable-line no-template-curly-in-string
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('DB_HOST')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('DB_USER')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('DB_PASSWORD')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('DB_NAME')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('DB_PREFIX')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('WP_HOME')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('WP_SITEURL')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('AUTH_KEY')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('SECURE_AUTH_KEY')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('LOGGED_IN_KEY')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('NONCE_KEY')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('AUTH_SALT')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('SECURE_AUTH_SALT')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('LOGGED_IN_SALT')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('NONCE_SALT')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('DB_HOST')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('DB_USER')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('DB_PASSWORD')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('DB_NAME')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('DB_PREFIX')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('WP_HOME')
      expect(dockerComposeYml.services['php-fpm'].environment).to.include('WP_SITEURL')
    })
  })

  it('fills docker-compose-run.yml with correct content', function () {
    const slug = path.basename(process.cwd())

    return yaml.read(process.cwd() + '/docker-compose-run.yml').then((dockerComposeYml) => {
      expect(dockerComposeYml.services['php-cli'].container_name).to.equal(slug + '-php-cli')
      expect(dockerComposeYml.services['php-cli'].volumes).to.include('${PROJECT_DIR}:/project') // eslint-disable-line no-template-curly-in-string
    })
  })

  it('fills config/nginx/site.conf with correct content', function () {
    assert.fileContent('config/nginx/site.conf', 'root /site/public')
  })

  it('fills wp-cli.yml with correct content', function () {
    assert.fileContent('wp-cli.yml', 'path: site/public')
  })

  it('fills composer.json with correct content', function () {
    const slug = path.basename(process.cwd())

    assert.JSONFileContent('composer.json', {
      name: `${slug}/${slug}`,
      type: 'project',
      keywords: ['wordpress', 'wysiwygoy'],
      require: {
        'vlucas/phpdotenv': '^2.4.0'
      }
    })
  })

  it('fills .env with correct content', function () {
    const slug = path.basename(process.cwd())

    assert.fileContent('.env', `COMPOSE_PROJECT_NAME=${slug}`)
    assert.fileContent('.env', `DB_HOST=${slug}-db`)
    assert.fileContent('.env', `WP_HOME=http://${slug}.local.wysiwyg.fi`)
    assert.fileContent('.env', `WP_SITEURL=http://${slug}.local.wysiwyg.fi/wordpress`)
    assert.fileContent('.env', `DB_HOST=${slug}-db`)
    assert.fileContent('.env', `DB_PREFIX=myprefix`)
  })

  it('fills site/public/index.php with correct content', function () {
    const slug = path.basename(process.cwd())

    assert.fileContent('site/public/index.php', `* @package ${slug}`)
  })

  it('fills wp-cli.yml with correct content', function () {
    assert.fileContent('wp-cli.yml', 'path: site/public/wordpress')
  })

  it('fills config/deploy-hosts with correct content', function () {
    assert.fileContent('config/deploy-hosts', 'mydeployhost ansible_user=mydeployuser')
  })
})
