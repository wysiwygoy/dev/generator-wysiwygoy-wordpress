Yeoman WordPress Project Generator
==================================

Yeoman generator for instantiating a new WordPress project.

Using with Docker
-----------------

```bash
docker run -i -v $PWD:/project registry.gitlab.com/wysiwygoy/dev/generator-wysiwygoy-wordpress wysiwyg|traditional|bedrock
```

Using with Node
---------------

First, install [Yeoman](http://yeoman.io) and generator-wysiwygoy-wordpress using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g gitlab:wysiwygoy/dev/generator-wysiwygoy-wordpress
```

Then generate your new project:

```bash
cd my-new-project-folder
yo wysiwygoy-wordpress wysiwyg|traditional|bedrock
```

Running integration tests
-------------------------

```bash
composer test-integration
```

Or via NPM (requires composer): 

```bash
npm run test-integration
```


