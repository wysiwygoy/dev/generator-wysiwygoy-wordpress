<?php
/**
 * Integration tests for generated wp-config.php in wysiwyg project model
 *
 * Requires the generated wp-config.php and tests that it compiles
 * and defines correct values.
 *
 * @package WysiwygOy\GeneratorWysiwygOyWordPress\Wysiwyg
 */

namespace WysiwygOy\GeneratorWysiwygOyWordPress\Wysiwyg;

use WysiwygOy\GeneratorWysiwygOyWordPress\Wp_Config_Test_Base;

/**
 * Integration tests for generated wp-config.php in wysiwyg project model.
 *
 * @covers wp-config.php
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
final class Wp_Config_Test extends Wp_Config_Test_Base {
	/**
	 * Constructor.
	 *
	 * @param null|string $name Optional. Name of the test case.
	 */
	function __construct( $name = null ) {
		parent::__construct( $name, 'wysiwyg' );
	}

	/**
	 * Overrides the parent setUp to also create a fake wp-settings.php file.
	 */
	protected function setUp() {
		parent::setUp();

		// @codingStandardsIgnoreLine
		copy( dirname( __FILE__, 2 ) . '/fake-wp-settings.php', "$this->project_dir/site/public/wordpress/wp-settings.php" );
	}

	/**
	 * Tests that when wp-config.php is required, it produces correct constant and variable values.
	 */
	public function testWysiwygWpConfigSetsCorrectValues() {
		global $www_dir;
		require( 'site/public/wp-config.php' );

		$this->assertEquals( "$www_dir/wp-content", WP_CONTENT_DIR );
		$this->assertEquals( "$www_dir/wordpress/", ABSPATH );
	}

	/**
	 * Tests that the generated index.php conforms to WordPress coding style.
	 */
	public function testIndexConformsToWpCodingStyle() {
		$output = array();
		// @codingStandardsIgnoreLine
		exec( 'site/vendor/bin/phpcs --standard=WordPress -q site/public/index.php', $output, $status );

		$this->assertEmpty( $output, join( "\n", $output ) );
		$this->assertEquals( 0, $status );
	}
}
