<?php
/**
 * Fake wp-settings.php file for generated wp-config.php integration tests
 *
 * Because wp-config.php requires wp-settings.php and we don't have it
 * generated, we provide this fake one that does nothing.
 *
 * @package WysiwygOy\GeneratorWysiwygOyWordPress
 */

/* Does nothing. */
