<?php
/**
 * Integration tests for generated wp-config.php in traditional project model
 *
 * Requires the generated wp-config.php and tests that it compiles
 * and defines correct values.
 *
 * @package WysiwygOy\GeneratorWysiwygOyWordPress\Wysiwyg
 */

namespace WysiwygOy\GeneratorWysiwygOyWordPress\Traditional;

use \WysiwygOy\GeneratorWysiwygOyWordPress\Wp_Config_Test_Base;

/**
 * Integration tests for generated wp-config.php in traditional project model.
 *
 * @covers wp-config.php
 * @runTestsInSeparateProcesses
 * @preserveGlobalState disabled
 */
final class Wp_Config_Test extends Wp_Config_Test_Base {
	/**
	 * Constructor.
	 *
	 * @param null|string $name Optional. Name of the test case.
	 */
	function __construct( $name = null ) {
		parent::__construct( $name, 'traditional' );
	}

	/**
	 * Overrides the parent setUp to also create a fake wp-settings.php file.
	 */
	protected function setUp() {
		parent::setUp();

		// @codingStandardsIgnoreLine
		copy( dirname( __FILE__, 2 ) . '/fake-wp-settings.php', "$this->project_dir/site/public/wp-settings.php" );
	}
}
