<?php
/**
 * Base class for generated wp-config.php integration tests
 *
 * @package WysiwygOy\GeneratorWysiwygOyWordPress
 */

namespace WysiwygOy\GeneratorWysiwygOyWordPress;

use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;

/**
 * Base class for generated wp-config.php integration tests.
 */
abstract class Wp_Config_Test_Base extends TestCase {
	/**
	 * Temporary directory for the tests.
	 *
	 * @var String $tmp_dir Temporary directory.
	 */
	private $tmp_dir;

	/**
	 * Project model to use.
	 *
	 * @var String $project_model Project model.
	 */
	private $project_model;

	/**
	 * Directory where the project scaffolding is generated.
	 *
	 * @var String $project_dir Project directory.
	 */
	protected $project_dir;

	/**
	 * Constructor.
	 *
	 * @param null|string $name Name of the test case.
	 * @param String      $project_model Model of project to generate.
	 */
	function __construct( $name, $project_model ) {
		parent::__construct( $name );
		$this->project_model = $project_model;
	}

	/**
	 * Creates the project scaffolding in a temporary directory.
	 *
	 * @throws Exception When executing yo or composer fails.
	 */
	protected function setUp() {
		// @codingStandardsIgnoreLine
		$this->tmp_dir = tempnam( sys_get_temp_dir(), "WpConfigTest-$this->project_model-" );
		$this->project_dir = $this->tmp_dir . '/myproject';

		// @codingStandardsIgnoreStart
		unlink( $this->tmp_dir );
		mkdir( $this->tmp_dir );
		mkdir( $this->project_dir );
		chdir( $this->project_dir );
		// @codingStandardsIgnoreEnd

		$output = array();
		// @codingStandardsIgnoreLine
		exec( "yo wysiwygoy-wordpress $this->project_model 2> /dev/null", $output, $status );
		if ( 0 !== $status ) {
			throw new Exception( 'yo failed' );
		}

		$output = array();
		// @codingStandardsIgnoreLine
		exec( 'composer install 2> /dev/null', $output, $status );
		if ( 0 !== $status ) {
			throw new Exception( 'composer failed' );
		}

		$dotenv = new Dotenv( $this->project_dir );
		$dotenv->load();
	}

	/**
	 * Removes the temporary directory.
	 */
	protected function tearDown() {
		rmrdir( $this->tmp_dir );
	}

	/**
	 * Tests that when wp-config.php is required, it produces correct constant and variable values.
	 */
	public function testWpConfigSetsCorrectDatabaseConstants() {
		global $www_dir;
		require( 'site/public/wp-config.php' );

		$this->assertEquals( "$this->project_dir/site/public", $www_dir );
		$this->assertEquals( 'myproject-db', DB_HOST );
		$this->assertEquals( 'myproject_wp_dev', DB_NAME );
	}

	/**
	 * Tests that the generated wp-config.php conforms to WordPress coding style.
	 */
	public function testWpConfigConformsToWpCodingStyle() {
		$output = array();
		// @codingStandardsIgnoreLine
		exec( 'site/vendor/bin/phpcs --standard=WordPress -q site/public/wp-config.php', $output, $status );

		$this->assertEmpty( $output, join( "\n", $output ) );
		$this->assertEquals( 0, $status );
	}
}
